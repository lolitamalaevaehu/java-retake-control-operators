package com.epam.rd.autotasks.pizzasplit;

import java.util.Scanner;

public class PizzaSplit {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numberOfPeople = scan.nextInt();
        int piecesPerPizza = scan.nextInt();

        int numberOfPizza = 1;
        while((numberOfPizza*piecesPerPizza)%numberOfPeople!=0){
            numberOfPizza++;
        }
        System.out.println(numberOfPizza);
    }
}
