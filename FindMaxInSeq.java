package com.epam.rd.autotasks.sequence;

import java.util.Scanner;

public class FindMaxInSeq {
    public static int max() {
        Scanner scan = new Scanner(System.in);
        int max = scan.nextInt();

        // Если первое число — ноль, возвращаем его и завершаем.
        if (max == 0) return 0;

        while (true) {
            int value = scan.nextInt();
            if (value == 0)
                break;
            if (value > max)
                max = value;
        }

        return max;
    }

    public static void main(String[] args) {

        System.out.println("Test your code here!\n");

        // Get a result of your code

        System.out.println(max());
    }
}
