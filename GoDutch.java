package com.epam.rd.autotasks.godutch;

import java.util.Scanner;

public class GoDutch {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int billTotal = scan.nextInt();
        int friendCount = scan.nextInt();

        if (billTotal < 0) {
            System.out.println("Bill total amount cannot be negative");
            return;
        }

        if (friendCount <= 0) {
            System.out.println("Number of friends cannot be negative or zero");
            return;
        }

        int totalToPay = billTotal + (int)(billTotal * 0.1);
        System.out.println(totalToPay / friendCount);
    }
}
