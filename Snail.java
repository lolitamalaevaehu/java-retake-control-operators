package com.epam.rd.autotasks.snail;

import java.util.Scanner;

public class Snail {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int h = scan.nextInt();

        if (a >= h) {
            System.out.println(1);
        }
        else if (a - b <= 0) {
            System.out.println("Impossible");
        }
        else {
            int days = (h - b - 1) / (a - b) + 1;
            System.out.println(days);
        }
    }

}
