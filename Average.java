package com.epam.rd.autotasks;

import java.util.Scanner;

public class Average {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int averageSummator = scanner.nextInt();
        int loopCounter = 0;
        if(averageSummator == 0) return; //Первое введеное число - не ноль.

        while(true){
            int value = scanner.nextInt();
            averageSummator+=value;
            loopCounter++;
            if(value == 0) break;
        }
        System.out.println(averageSummator/loopCounter);
    }

}